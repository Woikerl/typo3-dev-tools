<?php
declare(strict_types=1);
namespace Cyberhouse\Typo3DevTools\Fixer;

/*
 * This file is (c) 2018 by Georg Großberger
 * <contact@grossberger-ge.org> - <https://grossberger-ge.org/>
 *
 * It is free software; you can redistribute it and/or
 * modify it under the terms of the Apache License 2.0
 *
 * For the full copyright and license information see
 * the file LICENSE distributed with the source code
 * or <https://www.apache.org/licenses/LICENSE-2.0>
 */

use PhpCsFixer\Fixer\FixerInterface;
use PhpCsFixer\Tokenizer\Tokens;
use PhpCsFixer\Utils;
use SplFileInfo;

/**
 * Shared implementations of custom fixers
 *
 * @author Georg Großberger <contact@grossberger-ge.org>
 */
abstract class BaseFixer implements FixerInterface
{
    /**
     * {@inheritdoc}
     */
    public function isCandidate(Tokens $tokens): bool
    {
        return $tokens->isMonolithicPhp();
    }

    /**
     * {@inheritdoc}
     */
    public function isRisky(): bool
    {
        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function getPriority(): int
    {
        return 0;
    }

    /**
     * {@inheritdoc}
     */
    public function supports(SplFileInfo $file): bool
    {
        return 'php' === $file->getExtension();
    }

    /**
     * {@inheritdoc}
     */
    public function getName(): string
    {
        $nameParts = explode('\\', get_class($this));
        $name = substr(end($nameParts), 0, -strlen('Fixer'));

        return 'Cyberhouse/' . Utils::camelCaseToUnderscore($name);
    }
}
