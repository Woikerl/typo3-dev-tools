<?php
declare(strict_types=1);
namespace Cyberhouse\Typo3DevTools\Fixer;

/*
 * This file is (c) 2018 by Georg Großberger
 * <contact@grossberger-ge.org> - <https://grossberger-ge.org/>
 *
 * It is free software; you can redistribute it and/or
 * modify it under the terms of the Apache License 2.0
 *
 * For the full copyright and license information see
 * the file LICENSE distributed with the source code
 * or <https://www.apache.org/licenses/LICENSE-2.0>
 */

use PhpCsFixer\FixerDefinition\FixerDefinition;
use PhpCsFixer\FixerDefinition\FixerDefinitionInterface;
use PhpCsFixer\Tokenizer\Token;
use PhpCsFixer\Tokenizer\Tokens;
use SplFileInfo;

/**
 * @author Georg Großberger <contact@grossberger-ge.org>
 */
class SingleEmptyLineFixer extends BaseFixer
{
    public function getDefinition(): FixerDefinitionInterface
    {
        return new FixerDefinition(
            'Ensures there are no consecutive empty lines',
            []
        );
    }

    public function fix(SplFileInfo $file, Tokens $tokens): void
    {
        for ($i = 0; $i < $tokens->count(); $i++) {
            if ($tokens[$i]->isGivenKind(T_WHITESPACE)) {
                if (false !== strpos($tokens[$i]->getContent(), "\n")) {
                    $content = explode("\n", $tokens[$i]->getContent());

                    if (count($content) > 3) {
                        $content = array_slice($content, -3);
                    }

                    $tokens[$i] = new Token([T_WHITESPACE, implode("\n", $content)]);
                }
            }
        }

        $tokens->clearEmptyTokens();
    }
}
