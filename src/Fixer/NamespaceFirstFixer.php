<?php
declare(strict_types=1);
namespace Cyberhouse\Typo3DevTools\Fixer;

/*
 * This file is (c) 2018 by Georg Großberger
 * <contact@grossberger-ge.org> - <https://grossberger-ge.org/>
 *
 * It is free software; you can redistribute it and/or
 * modify it under the terms of the Apache License 2.0
 *
 * For the full copyright and license information see
 * the file LICENSE distributed with the source code
 * or <https://www.apache.org/licenses/LICENSE-2.0>
 */

use PhpCsFixer\FixerDefinition\FixerDefinition;
use PhpCsFixer\FixerDefinition\FixerDefinitionInterface;
use PhpCsFixer\Tokenizer\Token;
use PhpCsFixer\Tokenizer\Tokens;

/**
 * @author Georg Großberger <contact@grossberger-ge.org>
 */
class NamespaceFirstFixer extends BaseFixer
{
    public function getDefinition(): FixerDefinitionInterface
    {
        return new FixerDefinition(
            'Ensures the namespace to be the first element in a class file',
            []
        );
    }

    public function fix(\SplFileInfo $file, Tokens $tokens): void
    {
        $namespace = null;
        $insertAt = 1;
        $replaces = [];

        for ($i = 0; $i < count($tokens); $i++) {
            if ($tokens[$i]->isGivenKind(T_DECLARE)) {
                while (true) {
                    if ($i > count($tokens)) {
                        return;
                    }

                    $i++;
                    $insertAt = $i;

                    if ($tokens[$i - 1]->getContent() === ';') {
                        $i--;
                        break;
                    }
                }

                if ("\n" === $tokens[$i + 1]->getContent()) {
                    $insertAt++;
                }
            } elseif ($tokens[$i]->isGivenKind(T_NAMESPACE)) {
                $namespace = [new Token([T_NAMESPACE, 'namespace'])];
                $replaces[] = $i;
            } elseif (is_array($namespace)) {
                $namespace[] = $tokens[$i];
                $replaces[] = $i;

                if ($tokens[$i]->getContent() === ';') {
                    break;
                }
            }
        }

        if (1 === $insertAt) {
            $tokens[0] = new Token([T_OPEN_TAG, "<?php\n"]);
        }

        if (is_array($namespace)) {
            foreach ($replaces as $i) {
                $tokens[$i] = new Token('');
            }

            $tokens->clearEmptyTokens();
            $tokens->insertAt($insertAt, $namespace);

            $prev = $tokens[$insertAt - 1]->getContent();

            if (false === strpos($prev, "\n")  && $insertAt > 1) {
                $tokens->insertAt($insertAt, [new Token([T_WHITESPACE, "\n"])]);
            }
        }
    }
}
